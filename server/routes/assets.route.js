const express = require("express");
const authMiddleware = require("../middlewares/auth.middleware");
const assetsController = require("../controllers/assets.controller");

const router = express.Router();

// Routes
router.get("/", authMiddleware.userCheck, assetsController.getAssets);
router.get(
  "/asset/:assetId",
  authMiddleware.userCheck,
  assetsController.getAsset
);
router.post(
  "/create-asset",
  authMiddleware.adminCheck,
  assetsController.createAsset
);
router.post(
  "/edit-asset",
  authMiddleware.adminCheck,
  assetsController.editAsset
);

router.post(
  "/delete-asset",
  authMiddleware.adminCheck,
  assetsController.deleteAsset
);
router.post(
  "/add-sub-type",
  authMiddleware.adminCheck,
  assetsController.addSubType
);
router.get(
  "/sub-types",
  authMiddleware.userCheck,
  assetsController.getSubTypes
);

module.exports = router;
