const express = require("express");
const authMiddleware = require("../middlewares/auth.middleware");
const attendancesController = require("../controllers/attendances.controller");

const router = express.Router();

// Routes
router.get("/", authMiddleware.userCheck, attendancesController.getAttendances);
router.get(
  "/:attendanceId",
  authMiddleware.userCheck,
  attendancesController.getAttendance
);
router.post(
  "/create-attendance",
  authMiddleware.adminCheck,
  attendancesController.createAttendance
);

router.post(
  "/update-footage",
  authMiddleware.adminCheck,
  attendancesController.editEmployeeFootage
);

module.exports = router;
