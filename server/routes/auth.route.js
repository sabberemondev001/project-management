const express = require("express");
const authController = require("../controllers/auth.controller");
const authMiddleware = require("../middlewares/auth.middleware");

const router = express.Router();

// Routes
router.get("/", authController.initialize);
router.get("/info", authMiddleware.userCheck, authController.authInfoProvider);
router.post("/login", authController.login);
router.post("/signup-admin", authController.signUpAdmin);
router.post(
  "/create-user",
  authMiddleware.adminCheck,
  authController.createUser
);
router.post(
  "/update-user",
  authMiddleware.adminCheck,
  authController.updateUser
);

router.post(
  "/delete-user",
  authMiddleware.adminCheck,
  authController.deleteUser
);

router.post(
  "/update-profile",
  authMiddleware.userCheck,
  authController.updateProfile
);

router.get("/all-users", authMiddleware.adminCheck, authController.getAllUsers);

module.exports = router;
