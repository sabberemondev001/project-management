const express = require("express");
const authMiddleware = require("../middlewares/auth.middleware");
const billingController = require("../controllers/billings.controller");

const router = express.Router();

// Routes
router.get(
  "/contractor-costs/:projectId",
  billingController.getAllContractorCosts
);
router.get("/invoices/:projectId", billingController.getAllInvoices);

router.post(
  "/add-contractor-cost",
  authMiddleware.adminCheck,
  billingController.addContractorCost
);
router.post(
  "/add-invoice",
  authMiddleware.adminCheck,
  billingController.addInvoice
);

module.exports = router;
