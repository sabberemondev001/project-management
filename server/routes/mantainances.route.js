const express = require("express");
const authMiddleware = require("../middlewares/auth.middleware");
const mantainanceController = require("../controllers/mantainances.controller");

const router = express.Router();

// routes
router.post(
  "/create-mantainance",
  authMiddleware.adminCheck,
  mantainanceController.createMantainance
);

router.post(
  "/edit-mantainance",
  authMiddleware.adminCheck,
  mantainanceController.editMantainance
);

router.post(
  "/delete-mantainance",
  authMiddleware.adminCheck,
  mantainanceController.deleteMantainance
);

router.get(
  "/",
  authMiddleware.userCheck,
  mantainanceController.getMantainances
);

module.exports = router;
