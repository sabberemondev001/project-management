const express = require("express");
const authMiddleware = require("../middlewares/auth.middleware");
const employeesContoller = require("../controllers/employees.controller");

const router = express.Router();

// Routes
router.get("/", authMiddleware.userCheck, employeesContoller.getEmployees);
router.get(
  "/employee/:id",
  authMiddleware.userCheck,
  employeesContoller.getAnEmployee
);

router.post(
  "/create-employee",
  authMiddleware.adminCheck,
  employeesContoller.createEmployee
);

router.post(
  "/edit-employee",
  authMiddleware.adminCheck,
  employeesContoller.editEmployee
);

router.post(
  "/delete-employee",
  authMiddleware.adminCheck,
  employeesContoller.deleteEmployee
);

module.exports = router;
