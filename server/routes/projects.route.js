const express = require("express");
const authMiddleware = require("../middlewares/auth.middleware");
const projectsController = require("../controllers/projects.controller");

const router = express.Router();

// Routes
router.post("/add-task", authMiddleware.userCheck, projectsController.addTask);
router.post(
  "/edit-task",
  authMiddleware.userCheck,
  projectsController.editTask
);
router.post(
  "/create-project",
  authMiddleware.userCheck,
  projectsController.createProject
);
router.post(
  "/edit-project",
  authMiddleware.userCheck,
  projectsController.editProject
);
router.get("/", authMiddleware.userCheck, projectsController.getProjects);
router.get(
  "/project/:projectId",
  authMiddleware.userCheck,
  projectsController.getProject
);
router.get(
  "/contractors",
  authMiddleware.userCheck,
  projectsController.getContractors
);
router.post(
  "/contractors/add-contractor",
  authMiddleware.userCheck,
  projectsController.addContractor
);

router.post(
  "/contractors/edit-contractor",
  authMiddleware.userCheck,
  projectsController.editContractor
);

router.post(
  "/contractors/delete-contractor",
  authMiddleware.userCheck,
  projectsController.deleteContractor
);

module.exports = router;
