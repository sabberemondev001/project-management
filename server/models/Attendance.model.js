const mongoose = require("mongoose");

const AttendanceSchema = new mongoose.Schema(
  {
    month: {
      type: Number,
      required: true,
    },
    year: {
      type: Number,
      required: true,
    },
    employee_name: {
      type: String,
    },
    employee_id: {
      type: String,
    },
    records: {
      type: Array,
      required: true,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Attendance", AttendanceSchema);
