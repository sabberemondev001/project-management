const mongoose = require("mongoose");

const invoiceSchema = new mongoose.Schema(
  {
    project_id: {
      type: String,
      required: true,
    },
    task_id: {
      type: String,
      required: true,
    },
    project_name: {
      type: String,
    },
    task_name: {
      type: String,
    },
    type: {
      type: String, // possible values: 'Partial', 'Final'
    },
    invoice_number: {
      type: String,
    },
    invoice_date: {
      type: Date,
    },
    invoice_amount: {
      type: Number,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Invoice", invoiceSchema);
