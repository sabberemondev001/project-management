const mongoose = require("mongoose");

const contractorSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    phone_number: {
      type: String,
    },
    supervisor_name: {
      type: String,
    },
    supervisor_number: {
      type: String,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Contractor", contractorSchema);
