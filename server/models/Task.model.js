const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema(
  {
    project_id: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    estimated_start_date: {
      type: Date,
    },
    actual_start_date: {
      type: Date,
    },
    estimated_end_date: {
      type: Date,
    },
    actual_end_date: {
      type: Date,
    },
    days_count: {
      type: Number,
      default: 0,
    },
    actual_cost: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Task", taskSchema);
