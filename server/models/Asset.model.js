const mongoose = require("mongoose");

const assetSchema = new mongoose.Schema(
  {
    tag: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true, // possible values: "Vehicle", "Warehouse"
    },
    sub_type: {
      type: String,
    },
    tag_expiration: {
      type: Date,
    },
    assigned_to: {
      type: String,
      default: "",
    },
    vehicle_info: {
      type: {
        last_maintenance: {
          type: Date,
        },
        next_maintenance: {
          type: Date,
        },
        mileage: {
          type: Number,
        },
        loan_amount: {
          type: Number,
          default: 0,
        },
        monthly_loan_payment: {
          type: Number,
        },
        monthly_insurance_payment: {
          type: Number,
          default: 0,
        },
        daily_cost: {
          type: Number,
        },
        name: {
          type: String,
        },
        year: {
          type: String,
        },
      },
      required: false,
    },
    warehouse_info: {
      type: {
        name: {
          type: String,
        },
        location: {
          type: String,
        },
        city: {
          type: String,
        },
        state: {
          type: String,
        },
        zip: {
          type: String,
        },
        monthly_rent: {
          type: Number,
        },
        monthly_insurance_payment: {
          type: Number,
          default: 0,
        },
        monthly_utility_cost: {
          type: Number,
        },
      },
      required: false,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Asset", assetSchema);
