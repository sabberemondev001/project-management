const mongoose = require("mongoose");

const mantainanceSchema = new mongoose.Schema(
  {
    asset_id: {
      type: String,
      required: true,
    },
    asset_name: {
      type: String,
    },
    year: {
      type: String,
    },
    date_of_mantainance: {
      type: Date,
      required: true,
    },
    oil_change: {
      type: Boolean,
      default: false,
    },
    tire_rotation: {
      type: Boolean,
      default: false,
    },
    tire_change: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Mantainance", mantainanceSchema);
