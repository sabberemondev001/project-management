const mongoose = require("mongoose");

const projectSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    tasks: {
      type: Array,
      required: true,
    },
    cost_to_date: {
      type: Number,
      default: 0,
    },
    total_employee_cost: {
      type: Number,
      default: 0,
    },
    // new
    additional_cost: {
      type: Array,
      default: [],
    },
    invoiced_ammount: {
      type: Array,
      default: [],
    },
    contractors_cost: {
      type: Array,
      default: [],
    },
    total_progress: {
      type: Number,
      default: 0,
    },
    estimated_start_date: {
      type: Date,
    },
    region: {
      type: String,
    },
    total_cmh: {
      type: Number,
    },
    start_date: {
      type: Date,
    },
    days_count: {
      type: Number,
      default: 0,
    },
    end_date: {
      type: Date,
    },
    status: {
      type: String,
      default: "In Progress", // possible values: "In Progress", "Completed", "Cancelled", 'On Hold'
    },
    footages: {
      type: Array,
      default: [],
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Project", projectSchema);
