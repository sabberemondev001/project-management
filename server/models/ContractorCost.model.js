const mongoose = require("mongoose");

const contractorCostSchema = new mongoose.Schema(
  {
    invoice_number: {
      type: String,
      required: true,
    },
    porject_name: {
      type: String,
    },
    project_id: {
      type: String,
      required: true,
    },
    task_name: {
      type: String,
    },
    task_id: {
      type: String,
      required: true,
    },
    contractor_name: {
      type: String,
    },
    contractor_id: {
      type: String,
    },
    invoice_date: {
      type: Date,
    },
    invoice_amount: {
      type: Number,
    },
    job_completed: {
      type: Boolean,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("ContractorCost", contractorCostSchema);
