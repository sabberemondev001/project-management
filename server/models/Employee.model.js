const mongoose = require("mongoose");

const employeeSchema = new mongoose.Schema(
  {
    first_name: {
      type: String,
      required: true,
    },
    middle_name: {
      type: String,
    },
    last_name: {
      type: String,
    },
    overhead: {
      type: Boolean,
      default: false,
    },
    date_of_birth: {
      type: Date,
    },
    phone: {
      type: String,
    },
    home_address: {
      type: Object,
    },
    position: {
      type: String,
      required: true, // Possible values: "Driller", "Laborer", "Mini Operator", "Supervisor", "Cable Crew", "Locator"
    },
    daily_wage: {
      type: Number,
      required: true,
    },
    commission: {
      type: Number,
      default: 0,
    },
    join_date: {
      type: Date,
    },
    termination_date: {
      type: Date,
    },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Employee", employeeSchema);
