const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const logger = require("morgan");
const authRoute = require("./routes/auth.route");
const employeesRoute = require("./routes/employees.route");
const projectsRoute = require("./routes/projects.route");
const attendancesRoute = require("./routes/attendances.route");
const assetsRoute = require("./routes/assets.route");
const mantainancesRoute = require("./routes/mantainances.route");
const billingsRoute = require("./routes/billing.route");

// Constants
const PORT = global.process.env.PORT || 8000;

const app = express();

// Configurations
dotenv.config();

// Middlewares
app.use(cors());
app.use(logger("dev"));
app.use(bodyParser.json({ limit: "100mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "100mb" }));

// Routes
app.get("/", (req, res) => {
  res.status(200).json({ success: true, message: "Welcome to the API" });
});

app.use("/api/auth", authRoute);
app.use("/api/employees", employeesRoute);
app.use("/api/projects", projectsRoute);
app.use("/api/attendances", attendancesRoute);
app.use("/api/assets", assetsRoute);
app.use("/api/mantainances", mantainancesRoute);
app.use("/api/billings", billingsRoute);

// Database connection
mongoose
  .connect(global.process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    // Initialization
    app.listen(PORT, () => {
      console.log(`Server is listening on port ${PORT}`);
    });
  })
  .catch((err) => console.log("DB Connection err", err));
