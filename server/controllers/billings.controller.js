const ContractorCostModel = require("../models/ContractorCost.model");
const InvoiceModel = require("../models/Invoice.model");

// add contractor cost
const addContractorCost = async (req, res) => {
  try {
    const newContractorCost = await ContractorCostModel.create(req.body);

    return res.status(200).json({
      success: true,
      message: "Contractor cost added successfully",
      newContractorCost,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// get all contractor costs
const getAllContractorCosts = async (req, res) => {
  const { projectId } = req.params;
  try {
    const contractorCosts = await ContractorCostModel.find({
      project_id: projectId,
    });

    return res.status(200).json({
      success: true,
      message: "Contractor costs fetched successfully!",
      contractorCosts,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// add invoice
const addInvoice = async (req, res) => {
  try {
    const newInvoice = await InvoiceModel.create(req.body);

    return res.status(200).json({
      success: true,
      message: "Invoice added successfully",
      newInvoice,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// get all invoices
const getAllInvoices = async (req, res) => {
  const { projectId } = req.params;

  try {
    const invoices = await InvoiceModel.find({
      project_id: projectId,
    });

    return res.status(200).json({
      success: true,
      message: "Invoices fetched successfully!",
      invoices,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  addContractorCost,
  getAllContractorCosts,
  addInvoice,
  getAllInvoices,
};
