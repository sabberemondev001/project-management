const UserModel = require("../models/User.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Generate a token
const generateToken = (_id) => {
  return jwt.sign({ _id }, global.process.env.JWT_SECRET, {
    expiresIn: "30d",
  });
};

const initialize = async (req, res) => {
  try {
    // Checking if there is an admin user in the database
    const adminExists = await UserModel.findOne({ role: "admin" });

    if (adminExists) {
      return res.status(200).json({
        success: true,
        adminExists: true,
        message: "Admin account already exists",
      });
    }

    return res.status(200).json({
      success: true,
      adminExists: false,
      message: "Admin account does not exist",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const authInfoProvider = async (req, res) => {
  try {
    const user = await UserModel.findById(req.userId);

    if (!user)
      return res
        .status(400)
        .json({ success: false, message: "User not found" });

    return res.status(200).json({
      success: true,
      message: "Fetched auth info successfully",
      user: {
        username: user.username,
        role: user.role,
        name: user.name,
        authorized_projects: user.authorized_projects,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const signUpAdmin = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: "Missing username or password" });
  }

  try {
    // Checking again if there is an admin user in the database (just in case)

    const adminExists = await UserModel.findOne({ role: "admin" });

    if (adminExists) {
      return res.status(400).json({
        success: false,
        message: "Admin account already exists",
      });
    }

    // Hashing the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    // Creating new admin user
    const newAdminUser = await UserModel.create({
      username,
      password: hashedPassword,
      role: "admin",
    });
    // Generating token
    const token = generateToken(newAdminUser._id);

    return res.status(200).json({
      success: true,
      message: "Admin account created successfully",
      token,
      user: {
        username: newAdminUser.username,
        role: newAdminUser.role,
        name: newAdminUser.name,
        authorized_projects: newAdminUser.authorized_projects,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const createUser = async (req, res) => {
  const { username, password, authorized_projects, name, role } = req.body;

  if (!username || !password || !authorized_projects || !name || !role) {
    return res
      .status(400)
      .json({ success: false, message: "Missing required fields" });
  }

  try {
    // Checking if the username already exists
    const userExists = await UserModel.findOne({ username });

    if (userExists) {
      return res.status(400).json({
        success: false,
        message: "User account already exists",
      });
    }

    // Hashing the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    // Creating new user
    const newUser = await UserModel.create({
      username,
      name,
      role,
      password: hashedPassword,
      authorized_projects,
    });

    return res.status(200).json({
      success: true,
      message: "User account created successfully",
      user: {
        username: newUser.username,
        role: newUser.role,
        name: newUser.name,
        authorized_projects: newUser.authorized_projects,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const login = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res
      .status(400)
      .json({ success: false, message: "Missing username or password" });
  }

  try {
    // Checking if the username exists
    const user = await UserModel.findOne({ username });

    if (!user)
      return res
        .status(403)
        .json({ success: false, message: "Invalid username or password" });

    // Checking if the password is correct
    const isPasswordCorrect = await bcrypt.compare(password, user.password);

    if (!isPasswordCorrect)
      return res
        .status(403)
        .json({ success: false, message: "Invalid username or password" });

    // Generating token
    const token = generateToken(user._id);

    return res.status(200).json({
      success: true,
      message: "Login successful",
      token,
      user: {
        username: user.username,
        role: user.role,
        name: user.name,
        authorized_projects: user.authorized_projects,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const updateUser = async (req, res) => {
  const { username, password, role, authorized_projects, name } = req.body;

  if (!username || (!password && !role && !authorized_projects && !name)) {
    return res.status(400).json({
      success: false,
      message: "Username is missing or Nothing to update",
    });
  }
  try {
    // Checking if the user exists
    const user = await UserModel.findOne({ username });

    if (!user)
      return res.status(400).json({
        success: false,
        message: "User account does not exist",
      });

    let hashedPassword;
    if (password) {
      // Hashing the password
      const salt = await bcrypt.genSalt(10);
      hashedPassword = await bcrypt.hash(password, salt);
    }

    // Updating user
    const updatedUser = await UserModel.findByIdAndUpdate(
      user._id,
      {
        password: hashedPassword || user.password,
        role: role || user.role,
        name: name || user.name,
        authorized_projects: authorized_projects || user.authorized_projects,
      },
      { new: true }
    );

    return res.status(200).json({
      success: true,
      message: "User account updated successfully",
      user: {
        username: updatedUser.username,
        role: updatedUser.role,
        name: updatedUser.name,
        authorized_projects: updatedUser.authorized_projects,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const deleteUser = async (req, res) => {
  const { id } = req.body;

  if (!id) {
    return res.status(400).json({
      success: false,
      message: "Id is missing",
    });
  }

  try {
    // Checking if the user exists
    const user = await UserModel.findById(id);

    if (!user) {
      return res.status(400).json({
        success: false,
        message: "User account doesn't exist",
      });
    }

    // Deleting user
    await UserModel.findByIdAndDelete(id);

    return res.status(200).json({
      success: true,
      message: "User account deleted successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const getAllUsers = async (req, res) => {
  try {
    const users = await UserModel.find();

    return res.status(200).json({
      success: true,
      message: "Users fetched successfully!",
      users,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const updateProfile = async (req, res) => {
  const { password, name } = req.body;

  if (!password && !name) {
    return res.status(400).json({
      success: false,
      message: "Nothing to update",
    });
  }
  try {
    // Checking if the user exists
    const user = await UserModel.findById(req.userId);

    if (!user)
      return res.status(400).json({
        success: false,
        message: "User account does not exist",
      });

    let hashedPassword;
    if (password) {
      // Hashing the password
      const salt = await bcrypt.genSalt(10);
      hashedPassword = await bcrypt.hash(password, salt);
    }

    // Updating user
    const updatedUser = await UserModel.findByIdAndUpdate(
      user._id,
      {
        password: hashedPassword || user.password,
        name: name || user.name,
      },
      { new: true }
    );

    return res.status(200).json({
      success: true,
      message: "User profile updated successfully",
      user: {
        username: updatedUser.username,
        role: updatedUser.role,
        name: updatedUser.name,
        authorized_projects: updatedUser.authorized_projects,
      },
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  initialize,
  signUpAdmin,
  createUser,
  login,
  updateUser,
  authInfoProvider,
  getAllUsers,
  deleteUser,
  updateProfile,
};
