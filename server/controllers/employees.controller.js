const EmployeeModel = require("../models/Employee.model");

const createEmployee = async (req, res) => {
  if (!req.body) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const newEmployee = await EmployeeModel.create({
      ...req.body,
    });

    res.status(201).json({
      success: true,
      message: "Employee created successfully!",
      employee: newEmployee,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const editEmployee = async (req, res) => {
  const {
    id,
    first_name,
    middle_name,
    last_name,
    date_of_birth,
    phone,
    home_address,
    position,
    daily_wage,
    commission,
    join_date,
    termination_date,
    overhead,
  } = req.body;

  if (!id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const prevEmployee = await EmployeeModel.findById(id);

    if (!prevEmployee) {
      return res.status(404).json({
        success: false,
        message: "Employee not found!",
      });
    }

    const updatedEmployee = await EmployeeModel.findByIdAndUpdate(
      id,
      {
        first_name: first_name || prevEmployee.first_name,
        middle_name: middle_name || prevEmployee.middle_name,
        last_name: last_name || prevEmployee.last_name,
        date_of_birth: date_of_birth || prevEmployee.date_of_birth,
        phone: phone || prevEmployee.phone,
        home_address: home_address
          ? {
              street: home_address.street || prevEmployee.home_address.street,
              city: home_address.city || prevEmployee.home_address?.city,
              state: home_address.state || prevEmployee.home_address.state,
              zip: home_address.zip || prevEmployee.home_address.zip,
            }
          : prevEmployee.home_address,
        position: position || prevEmployee.position,
        daily_wage: daily_wage || prevEmployee.daily_wage,
        commission: commission || prevEmployee.commission,
        join_date: join_date || prevEmployee.join_date,
        termination_date: termination_date || prevEmployee.termination_date,
        overhead: overhead || prevEmployee.overhead,
      },
      { new: true }
    );

    res.status(200).json({
      success: true,
      message: "Employee updated successfully!",
      employee: updatedEmployee,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const deleteEmployee = async (req, res) => {
  const { id } = req.body;

  if (!id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    await EmployeeModel.findByIdAndDelete(id);

    res.status(200).json({
      success: true,
      message: "Employee deleted successfully!",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const getEmployees = async (req, res) => {
  try {
    const employees = await EmployeeModel.find();

    res.status(200).json({
      success: true,
      message: "Employees fetched successfully!",
      employees,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const getAnEmployee = async (req, res) => {
  const { id } = req.params;

  if (!id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const employee = await EmployeeModel.findById(id);

    return res.status(200).json({
      success: true,
      message: "Employee fetched successfully!",
      employee,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  createEmployee,
  editEmployee,
  deleteEmployee,
  getEmployees,
  getAnEmployee,
};
