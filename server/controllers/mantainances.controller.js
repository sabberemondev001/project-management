const MantainanceModel = require("../models/Mantainance.model");
const AssetModel = require("../models/Asset.model");

const createMantainance = async (req, res) => {
  const { asset_id, ...rest } = req.body;
  try {
    const asset = await AssetModel.findById(asset_id);

    if (!asset) {
      return res
        .status(404)
        .json({ success: false, message: "Asset not found" });
    }

    const mantainance = await MantainanceModel.create({
      asset_id,
      asset_name: asset?.vehicle_info?.name,
      year: asset?.vehicle_info?.year || "N/A",
      ...rest,
    });
    return res.status(201).json({
      success: true,
      message: "Mantainance created successfully",
      mantainance,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const editMantainance = async (req, res) => {
  const { id, ...rest } = req.body;

  if (!id) {
    return res
      .status(400)
      .json({ success: false, message: "Missing mantainance id" });
  }

  try {
    const editedMantainance = await MantainanceModel.findByIdAndUpdate(
      id,
      { ...rest },
      {
        new: true,
      }
    );

    if (!editedMantainance) {
      return res
        .status(404)
        .json({ success: false, message: "Mantainance not found" });
    }

    return res.status(200).json({
      success: true,
      message: "Mantainance edited successfully",
      mantainance: editedMantainance,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const getMantainances = async (req, res) => {
  try {
    const mantainances = await MantainanceModel.find();

    return res.status(200).json({
      success: true,
      message: "Mantainances retrieved successfully",
      mantainances,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const deleteMantainance = async (req, res) => {
  const { id } = req.body;

  if (!id) {
    return res
      .status(400)
      .json({ success: false, message: "Missing mantainance id" });
  }

  try {
    const deletedMantainance = await MantainanceModel.findByIdAndDelete(id);

    if (!deletedMantainance) {
      return res
        .status(404)
        .json({ success: false, message: "Mantainance not found" });
    }

    return res.status(200).json({
      success: true,
      message: "Mantainance deleted successfully",
      mantainance: deletedMantainance,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  createMantainance,
  editMantainance,
  getMantainances,
  deleteMantainance,
};
