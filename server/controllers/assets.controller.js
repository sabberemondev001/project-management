const AssetModel = require("../models/Asset.model");
const SubTypeModel = require("../models/SubType.model");

const createAsset = async (req, res) => {
  try {
    const asset = await AssetModel.create(req.body);
    return res.status(201).json({
      success: true,
      message: "Asset created successfully",
      asset,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const editAsset = async (req, res) => {
  const { id, ...rest } = req.body;

  if (!id) {
    return res
      .status(400)
      .json({ success: false, message: "Missing asset id" });
  }

  try {
    const editedAsset = await AssetModel.findByIdAndUpdate(
      id,
      { ...rest },
      {
        new: true,
      }
    );

    if (!editedAsset) {
      return res
        .status(404)
        .json({ success: false, message: "Asset not found" });
    }

    return res.status(200).json({
      success: true,
      message: "Asset edited successfully",
      asset: editedAsset,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const getAssets = async (req, res) => {
  try {
    const assets = await AssetModel.find();

    return res.status(200).json({
      success: true,
      message: "Assets retrieved successfully",
      assets,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const deleteAsset = async (req, res) => {
  const { id } = req.body;

  if (!id) {
    return res
      .status(400)
      .json({ success: false, message: "Missing asset id" });
  }

  try {
    const deletedAsset = await AssetModel.findByIdAndDelete(id);

    if (!deletedAsset) {
      return res
        .status(404)
        .json({ success: false, message: "Asset not found" });
    }

    return res.status(200).json({
      success: true,
      message: "Asset deleted successfully",
      asset: deletedAsset,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

// get a single asset
const getAsset = async (req, res) => {
  const { assetId } = req.params;

  if (!assetId) {
    return res
      .status(400)
      .json({ success: false, message: "Missing asset id" });
  }

  try {
    const asset = await AssetModel.findById(assetId);

    if (!asset) {
      return res
        .status(404)
        .json({ success: false, message: "Asset not found" });
    }

    return res.status(200).json({
      success: true,
      message: "Asset retrieved successfully",
      asset,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

// add a new sub type
const addSubType = async (req, res) => {
  const { name } = req.body;

  if (!name) {
    return res
      .status(400)
      .json({ success: false, message: "Missing sub type name" });
  }

  try {
    const subType = await SubTypeModel.create({ name });

    return res.status(201).json({
      success: true,
      message: "Sub type created successfully",
      subType,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

// get all sub types
const getSubTypes = async (req, res) => {
  try {
    const subTypes = await SubTypeModel.find();

    return res.status(200).json({
      success: true,
      message: "Sub types retrieved successfully",
      subTypes,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  createAsset,
  editAsset,
  getAssets,
  deleteAsset,
  getAsset,
  addSubType,
  getSubTypes,
};
