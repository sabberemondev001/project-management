const AttendanceModel = require("../models/Attendance.model");
const TaskModel = require("../models/Task.model");
const ProjectModel = require("../models/Project.model");

const createAttendance = async (req, res) => {
  const {
    month,
    year,
    employee_name,
    new_records,
    employee_id,
    employee_info,
    total_footage,
  } = req.body;

  // if (
  //   !month ||
  //   !year ||
  //   !employee_name ||
  //   !new_records ||
  //   !employee_id ||
  //   !employee_info
  // ) {
  //   return res.status(400).json({
  //     success: false,
  //     message: "Required fields are missing!",
  //   });
  // }

  try {
    // calculate the total days count and cost of the task based on the new records employee salay, daily salary

    if (new_records[0].attendance_status === "Present") {
      const existingTask = await TaskModel.findById(new_records[0].task_id);
      const existingProject = await ProjectModel.findById(
        new_records[0].project_id
      );

      if (!existingTask || !existingProject) {
        return res.status(400).json({
          success: false,
          message: "Task not found!",
        });
      }

      const commissionCost = total_footage
        ? Number(employee_info.commission) * Number(total_footage)
        : 0;
      const newCost =
        existingTask.actual_cost +
        Number(employee_info.daily_wage) +
        commissionCost;

      // update the task with the new cost and days count
      const updatedTask = await TaskModel.findByIdAndUpdate(
        new_records[0].task_id,
        {
          actual_cost: Math.ceil(newCost),
        },
        { new: true }
      );

      const updatedProject = await ProjectModel.findByIdAndUpdate(
        new_records[0].project_id,
        {
          total_employee_cost:
            existingProject.total_employee_cost +
            Number(employee_info.daily_wage) +
            commissionCost,
          footages: [
            ...existingProject.footages,
            {
              footage: total_footage,
              date: new_records[0].date,
              employee_name,
              employee_id,
              task_id: new_records[0].task_id,
              commission: employee_info.commission,
            },
          ],
        },
        { new: true }
      );

      console.log("updatedTask", updatedTask);
      console.log("updatedProject", updatedProject);
    }
    // checking if attendance already exists
    const existingAttendance = await AttendanceModel.findOne({
      month: month,
      year: year,
      employee_id: employee_id,
    });
    // if attendance exists, push new records to existing records
    if (existingAttendance) {
      const updatedRecords = [...existingAttendance.records, ...new_records];
      const updatedAttendance = await AttendanceModel.findOneAndUpdate(
        {
          month: month,
          year: year,
          employee_id: employee_id,
        },
        {
          records: updatedRecords,
        },
        {
          new: true,
        }
      );
      return res.status(200).json({
        success: true,
        message: "Attendance updated successfully!",
        attendance: updatedAttendance,
      });
    }

    const newAttendance = await AttendanceModel.create({
      month,
      year,
      employee_name,
      records: new_records,
      employee_id,
    });

    if (!newAttendance) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    return res.status(201).json({
      success: true,
      message: "Attendance created successfully!",
      attendance: newAttendance,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const getAttendances = async (req, res) => {
  const { month, year } = req.query;

  try {
    const attendances = await AttendanceModel.find({
      month: month,
      year: year,
    });

    if (!attendances) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    return res.status(200).json({
      success: true,
      message: "Attendances fetched successfully!",
      attendances,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const getAttendance = async (req, res) => {
  const { attendanceId } = req.params;

  try {
    const attendance = await AttendanceModel.findById(attendanceId);

    if (!attendance) {
      return res.status(400).json({
        success: false,
        message: "Attendance not found!",
      });
    }

    return res.status(200).json({
      success: true,
      message: "Attendance fetched successfully!",
      attendance,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

// edit employee footage for a date in a project
const editEmployeeFootage = async (req, res) => {
  const { projectId, footage, date } = req.body;

  try {
    const existingProject = await ProjectModel.findById(projectId);

    if (!existingProject) {
      return res.status(400).json({
        success: false,
        message: "Project not found!",
      });
    }

    const existingFootage = existingProject.footages.find(
      (footage) => footage.date === date
    );

    if (!existingFootage) {
      return res.status(400).json({
        success: false,
        message: "Footage not found!",
      });
    }

    // update task actual cost
    const existingTask = await TaskModel.findById(existingFootage.task_id);

    if (!existingTask) {
      return res.status(400).json({
        success: false,
        message: "Task not found!",
      });
    }

    const prevCommissionCost =
      Number(existingFootage.footage) * Number(existingFootage.commission);
    const newCommissionCost =
      Number(footage) * Number(existingFootage.commission);

    existingTask.actual_cost =
      existingTask.actual_cost - prevCommissionCost + newCommissionCost;

    await existingTask.save();

    existingFootage.footage = footage;

    const updatedProject = await ProjectModel.findByIdAndUpdate(
      projectId,
      {
        footages: [
          ...existingProject.footages.filter(
            (footage) => footage.date !== date
          ),
          existingFootage,
        ],
      },
      { new: true }
    );

    return res.status(200).json({
      success: true,
      message: "Footage updated successfully!",
      project: updatedProject,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  createAttendance,
  getAttendances,
  getAttendance,
  editEmployeeFootage,
};
