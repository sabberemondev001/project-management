const ProjectModel = require("../models/Project.model");
const TaskModel = require("../models/Task.model");
const ContractorModel = require("../models/Contractor.model");

const addTask = async (req, res) => {
  const { project_id, name } = req.body;

  if (!project_id || !name) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const newTask = await TaskModel.create({
      project_id,
      name,
    });

    if (!newTask) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    const updatedProject = await ProjectModel.findByIdAndUpdate(
      project_id,
      {
        $push: { tasks: { id: newTask._id, name: newTask.name } },
      },
      { new: true }
    );

    res.status(201).json({
      success: true,
      message: "Task created successfully!",
      task: newTask,
      project: updatedProject,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const editTask = async (req, res) => {
  const { task_id, new_days_count, new_additional_cost, ...rest } = req.body;

  if (!task_id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const existingtask = await TaskModel.findById(task_id);

    if (!existingtask) {
      return res
        .status(404)
        .json({ success: false, message: "Task not found" });
    }

    const updatedTask = await TaskModel.findByIdAndUpdate(
      task_id,
      {
        ...rest,
        days_count: new_days_count
          ? existingtask.days_count + Number(new_days_count)
          : existingtask.days_count,
        additional_cost: new_additional_cost
          ? existingtask.additional_cost + Number(new_additional_cost)
          : existingtask.additional_cost,
      },
      { new: true }
    );

    if (!updatedTask) {
      return res.status(500).json({ success: false, message: "Server error" });
    }

    return res.status(200).json({
      success: true,
      message: "Task updated successfully!",
      task: updatedTask,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Server error" });
  }
};

const createProject = async (req, res) => {
  if (!req.body) {
    return res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const defaultTask1 = await TaskModel.create({ name: "Drilling" });
    const defaultTask2 = await TaskModel.create({ name: "Missiles" });
    const defaultTask3 = await TaskModel.create({ name: "Tie-Ins" });
    const defaultTask4 = await TaskModel.create({ name: "Cable Pulling" });
    const defaultTask5 = await TaskModel.create({ name: "Restoration" });
    const defaultTask6 = await TaskModel.create({ name: "Billing" });

    const newProject = await ProjectModel.create({
      ...req.body,
      tasks: [
        {
          id: defaultTask1._id,
          name: defaultTask1.name,
        },
        {
          id: defaultTask2._id,
          name: defaultTask2.name,
        },
        {
          id: defaultTask3._id,
          name: defaultTask3.name,
        },
        {
          id: defaultTask4._id,
          name: defaultTask4.name,
        },
        {
          id: defaultTask5._id,
          name: defaultTask5.name,
        },
        {
          id: defaultTask6._id,
          name: defaultTask6.name,
        },
      ],
    });

    if (!newProject) {
      return res.status(500).json({ success: false, message: "Server error" });
    }

    return res.status(201).json({
      success: true,
      message: "Project created successfully!",
      project: newProject,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const editProject = async (req, res) => {
  const {
    project_id,
    new_additional_cost,
    new_invoiced_ammount,
    new_days_count,
    name,
    status,
    start_date,
    end_date,
    region,
    total_cmh,
    franchise_name,
    estimated_start_date,
  } = req.body;

  if (!project_id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const existingProject = await ProjectModel.findById(project_id);

    const updatedProject = await ProjectModel.findByIdAndUpdate(
      project_id,
      {
        name: name || existingProject.name,
        region: region || existingProject.region,
        total_cmh: total_cmh || existingProject.total_cmh,
        franchise_name: franchise_name || existingProject.franchise_name,
        estimated_start_date:
          estimated_start_date || existingProject.estimated_start_date,
        status: status || existingProject.status,
        start_date: start_date || existingProject.start_date,
        end_date: end_date || existingProject.end_date,
        invoiced_ammount: new_invoiced_ammount
          ? existingProject.invoiced_ammount + Number(new_invoiced_ammount)
          : existingProject.invoiced_ammount,
        additional_cost: new_additional_cost
          ? [...existingProject.additional_cost, new_additional_cost]
          : existingProject.additional_cost,
        days_count: new_days_count
          ? existingProject.days_count + Number(new_days_count)
          : existingProject.days_count,
      },

      { new: true }
    );

    if (!updatedProject) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    res.status(200).json({
      success: true,
      message: "Project updated successfully!",
      project: updatedProject,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const getProjects = async (req, res) => {
  try {
    const projects = await ProjectModel.find().populate("tasks");
    // const projects = await ProjectModel.find();

    if (!projects) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    res.status(200).json({
      success: true,
      message: "Projects fetched successfully!",
      projects,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

const getProject = async (req, res) => {
  const { projectId } = req.params;

  if (!projectId) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const project = await ProjectModel.findById(projectId);

    if (!project) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    const tasks = project.tasks.map(async (task) => {
      const taskDetails = await TaskModel.findById(task.id);
      return taskDetails;
    });

    const resolvedTasks = await Promise.all(tasks);

    res.status(200).json({
      success: true,
      message: "Project fetched successfully!",
      project,
      tasks: resolvedTasks,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// add contractor to project
const addContractor = async (req, res) => {
  try {
    const newContractor = await ContractorModel.create({
      ...req.body,
    });

    return res.status(201).json({
      success: true,
      message: "Contractor created successfully!",
      contractor: newContractor,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// get contractors
const getContractors = async (req, res) => {
  try {
    const contractors = await ContractorModel.find({});

    if (!contractors) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    res.status(200).json({
      success: true,
      message: "Contractors fetched successfully!",
      contractors,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// edit contractor
const editContractor = async (req, res) => {
  const { contractor_id, ...rest } = req.body;

  if (!contractor_id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const updatedContractor = await ContractorModel.findByIdAndUpdate(
      contractor_id,
      {
        ...rest,
      },
      { new: true }
    );

    if (!updatedContractor) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    res.status(200).json({
      success: true,
      message: "Contractor updated successfully!",
      contractor: updatedContractor,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

// delete contractor
const deleteContractor = async (req, res) => {
  const { contractor_id } = req.body;

  if (!contractor_id) {
    res.status(400).json({
      success: false,
      message: "Required fields are missing!",
    });
  }

  try {
    const deletedContractor = await ContractorModel.findByIdAndDelete(
      contractor_id
    );

    if (!deletedContractor) {
      res.status(500).json({ success: false, message: "Server error" });
    }

    res.status(200).json({
      success: true,
      message: "Contractor deleted successfully!",
      contractor: deletedContractor,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: "Server error" });
  }
};

module.exports = {
  addTask,
  createProject,
  editTask,
  getProjects,
  editProject,
  getProject,
  addContractor,
  getContractors,
  editContractor,
  deleteContractor,
};
